*** Settings ***
Documentation    0001 0009 0012 0013
Library     SeleniumLibrary
Library     OperatingSystem

*** Variables ***
${เว็บ}                 https://borrowing2024.000webhostapp.com/
${BROWSER}               Firefox
${btnSelectWordfile}     xpath=//input[@type="file"]
${imgFile}               ${CURDIR}${/}testfile.jpg

*** Test Cases ***
Login Test
    Open The Browser
    Maximize The Browser
    Enter Username And Password
    Waiting     2s
    Click To Login
    Waiting     2s
    Click Link
    Waiting     2s
    Click Insert
    Insert Data
    Waiting     4s
    # Modify Data  -- โค้ดถูกต้อง แต่มันไม่รัน
    Waiting     2s
    Delete Data
    Waiting     2s
    Close The Browser

*** Keywords ***
Open The Browser
    Open Browser     ${เว็บ}    ${BROWSER}
    Wait Until Element Is Visible    id:username

Maximize The Browser
    Maximize Browser Window

Enter Username And Password
    Input Text     id:username    admin
    Input Text     id:password    admin

Click To Login
    Click Element    css=button[type='submit']

Click Link
    Click Element    css:a[href="./ins_category.html"]

Click Insert
    Click Button    id:add_category_btn

Insert Data
    Input Text    id:categoryId    30
    Input Text    id:categoryName    เครื่องมือเอเลี่ยนประหลาด
    Input Text    id:measure    เรือน
    Choose File    id:categoryImg    ${imgFile}
    Click Element    id:action
    Waiting     2s
    Click Element    css=button.swal2-confirm

# Modify Data -- โค้ดถูกต้อง แต่มันไม่รัน
#     Click Element    xpath=//a[@class="page-link" and text()="2"]
#     Waiting     2s
#     Click Button    id:update-cate-30
#     Clear Element Text    id:categoryName
#     Waiting     2s
#     Input Text    id:categoryName    เครื่องมือเอเลี่ยนประหลาด2
#     Waiting     2s
#     Click Element    id:action

Delete Data
    Click Element    xpath=//a[@class="page-link" and text()="2"]
    Waiting     2s
    Click Button    id:delete-cate-30
    Waiting     2s
    Click Element    css=button.swal2-confirm.swal2-styled.swal2-default-outline
    Waiting     3s
    Click Element    css=button.swal2-confirm
    
Waiting 
    [Arguments]     ${time}
    Sleep     ${time}

Close The Browser
    Close Browser
