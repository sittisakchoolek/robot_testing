*** Settings ***
Documentation    0001 0009 0012 0013
Library    SeleniumLibrary

*** Variables ***
${WEB URL}    https://borrowing2024.000webhostapp.com/
${BROWSER}    chrome
${USERNAME}    admin
${PASSWORD}    admin

*** Test Cases ***
Login
    Open Website
Start Test Case
    Fill Form Content
    Reserve Equip
    Exit

*** Keywords ***
Open Website
    Open Browser    ${WEB URL}    ${BROWSER}
    Maximize Browser Window
    Sleep    3s
    Wait Until Element Is Visible    id:username
    Input Text    id:username    ${USERNAME}
    Sleep    1s
    Input Password    id:password    ${PASSWORD}
    Sleep    1s
    Click Element    css=button[type='submit']
    Sleep    1s
Fill Form Content
    Input Text    id:title    งานเลี้ยงโต้รุ่ง
    Input Text    id:telNumber    0562233698
    Input Text    id:borrowDate    03-26-2024
    Input Text    id:returnDate    03-29-2024
    Input Text    id:objective    Get grade A from Software Testing
    Sleep    3s
Reserve Equip
    # Add Item 1
    Wait Until Element Is Visible    id=openEquipmentListModal    10
    Click Element    id:openEquipmentListModal
    Sleep    3s
    Input Text    id:reserveQty-5    3
    Sleep    2s
    Click Element    id:reserve-equip-id-5
    Sleep    3s
    Click Element    css=button[class='swal2-confirm swal2-styled']
    # Add Item 2
    Wait Until Element Is Visible    id=openEquipmentListModal    10
    Click Element    id:openEquipmentListModal
    Sleep    3s
    Input Text    id:reserveQty-2    5
    Sleep    2s
    Click Element    id:reserve-equip-id-2
    Sleep    3s
    Click Element    css=button[class='swal2-confirm swal2-styled']
    Sleep    3s
    # Add Item 3
    Wait Until Element Is Visible    id=openEquipmentListModal    10
    Click Element    id:openEquipmentListModal
    Sleep    3s
    Input Text    id:reserveQty-6    2
    Sleep    3s
    Click Element    id:reserve-equip-id-6
    Sleep    3s
    Click Element    css=button[class='swal2-confirm swal2-styled']
    Sleep    3s
    # Submit
    Click Element    css=input[class='btn btn-primary']
    Sleep    2s
Exit
    Close Browser
