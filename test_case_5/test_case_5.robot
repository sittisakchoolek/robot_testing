*** Settings ***
Documentation    0001 0009 0012 0013
Library     SeleniumLibrary

*** Variables ***
${เว็บ}                 https://borrowing2024.000webhostapp.com/
${BROWSER}               Firefox

*** Test Cases ***
Login Test
    Open The Browser
    Maximize The Browser
    Enter Username And Password
    Waiting     2s
    Click To Login
    Waiting     2s
    Click Link
    Waiting     2s
    Return
    Waiting     2s
    Close The Browser

*** Keywords ***
Open The Browser
    Open Browser     ${เว็บ}    ${BROWSER}
    Wait Until Element Is Visible    id:username

Maximize The Browser
    Maximize Browser Window

Enter Username And Password
    Input Text     id:username    admin
    Input Text     id:password    admin

Click To Login
    Click Element    css=button[type='submit']

Click Link
    Click Element    css:a[href="./return_equipment.html"]

Return
    Click Button    id:return-6-5
    Waiting     2s
    Click Element    css=button.swal2-confirm
    Waiting     2s
    Click Button    id:return-4-3
    Waiting     2s
    Click Element    css=button.swal2-confirm
Waiting 
    [Arguments]     ${time}
    Sleep     ${time}

Close The Browser
    Close Browser
